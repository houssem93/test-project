/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaapplication1;

/**
 *
 * @author houss
 */
public class Algo {
/*
  You are given N counters, initially set to 0, and you have two possible operations on them:
  
  • increase(X) − counter X is increased by 1,
  • max counter − all counters are set to the maximum value of any counter.
  
  A non-empty zero-indexed array A of M integers is given. This array represents consecutive operations:
  
  • if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
  • if A[K] = N + 1 then operation K is max counter.
  
  Write a function  that, given an integer N and a non-empty zero-indexed array A consisting of M integers, 
  returns a sequence of integers representing the values of the counters.
*/

public int[] solution(int N, int[] A) {
    final int condition = N + 1;
        int currentMax = 0;
        int lastUpdate = 0;
        int countersArray[] = new int[N];

        for (int iii = 0; iii < A.length; iii++) {
            int currentValue = A[iii];
            if (currentValue == condition) {
                lastUpdate = currentMax;
            } else {
                int position = currentValue - 1;
                if (countersArray[position] < lastUpdate)
                    countersArray[position] = lastUpdate + 1;
                else
                    countersArray[position]++;

                if (countersArray[position] > currentMax) {
                    currentMax = countersArray[position];
                }
            }

        }

        for (int iii = 0; iii < N; iii++) {
           if (countersArray[iii] < lastUpdate)
               countersArray[iii] = lastUpdate;
        }

        return countersArray;
    }

}
